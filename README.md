# Django Web Polls


<p align="center">
  <a href="https://travis-ci.org/sodrooome/website">
    <img src="https://travis-ci.org/sodrooome/website.svg?branch=master"
         alt="Gitter">
  </a>
  <a href="https://requires.io/github/sodrooome/django-web-polls/requirements/?branch=master">
      <img src="https://requires.io/github/sodrooome/django-web-polls/requirements.svg?branch=master">
  </a>
    <img src="https://img.shields.io/pypi/pyversions/django.svg">
  </a>
  </a>
  <a href="https://app.fossa.io/projects/git%2Bgithub.com%2Fsodrooome%2Fdjango-web-polls?ref=badge_shield">
    <img src="https://app.fossa.io/api/projects/git%2Bgithub.com%2Fsodrooome%2Fdjango-web-polls.svg?type=shield">
  </a>
</p>
<br>

a simple package or user libraries useful to take vote on web-based polls built for django framework

# Installation

- For live test only

`pip install -i https://test.pypi.org/simple/ django-web-polls` 

- For implemented in project

`pip install django-web-polls`

# Verifying

go to Python prompt and enter the command like this :

`import django-web-polls`

# License

BSD 3
